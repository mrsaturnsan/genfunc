#ifndef GENFUNC_HPP
#define GENFUNC_HPP

#include <string>
#include <unordered_map>
#include <variant>

template <typename... Callables>
class GenFunc
{
private:
    std::unordered_multimap<std::string, std::variant<Callables...>> m_callables;
public:
    template <typename T>
    void AddCallable(const std::string& name, const T& callable)
    {
        m_callables.emplace(name, std::variant<Callables...>{callable});
    }

    template <typename T>
    void AddCallable(std::string&& name, const T& callable)
    {
        m_callables.emplace(name, std::variant<Callables...>{callable});
    }

    template <typename T>
    void AddCallable(const std::string& name, T&& callable)
    {
        m_callables.emplace(name, std::variant<Callables...>{std::forward<T>(callable)});
    }

    template <typename T>
    void AddCallable(std::string&& name, T&& callable)
    {
        m_callables.emplace(name, std::variant<Callables...>{std::forward<T>(callable)});
    }

    void RemoveCallables(const std::string& name)
    {
        m_callables.erase(name);
    }

    template <typename... Args>
    void Call(const std::string& name, Args&&... args)
    {
        auto ret = m_callables.equal_range(name);

        for (auto it = ret.first; it != ret.second; ++it)
        {
            std::visit([&] (auto&& callable) {
                if constexpr (std::is_pointer_v<decltype(callable)>)
                {
                    (*callable)(args...);
                }
                else
                {
                    callable(args...);
                }
            }, it->second);
        }
    }
};

#endif
