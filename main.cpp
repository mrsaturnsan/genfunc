#include "GenFunc.hpp"
#include "test.hpp"
#include <iostream>

namespace
{
    int i = 0;
    float f = 0.0f;
    double d = 0.0;
    char c = '0';

    void setI(int i_)
    {
        i = i_;
    }

    void setF(float f_)
    {
        f = f_;
    }

    void setD(double d_)
    {
        d = d_;
    }

    void setC(char c_)
    {
        c = c_;
    }
}

int main()
{
    GenFunc<decltype(&setI), decltype(&setF), decltype(&setD), decltype(&setC)> g;

    g.AddCallable("setI", &setI);
    g.AddCallable("setF", &setF);
    g.AddCallable("setD", &setD);
    g.AddCallable("setC", &setC);

    g.Call("setI", 5);
    EXPECT_EQ(i, 5);

    g.Call("setF", -43.f);
    EXPECT_EQ(f, -43.f);

    g.Call("setD", 43434.2);
    EXPECT_EQ(d, 43434.2);

    g.Call("setC", 'z');
    EXPECT_EQ(c, 'z');

    struct TestStruct
    {
        void operator()(int& prm) const noexcept
        {
            prm += 23;
        }
    };

    auto test_lambda = [] (int& prm) noexcept {
        prm -= 3;
    };

    GenFunc<TestStruct, decltype(test_lambda)> g2;

    TestStruct ts;
    g2.AddCallable("callTest", std::move(ts));

    g2.Call("callTest", i);
    EXPECT_EQ(i, 28);

    EXPECT_NO_THROW(
        g2.RemoveCallables("callTest");
    )

    g2.AddCallable("callTest", std::move(test_lambda));
    
    g2.Call("callTest", i);
    EXPECT_EQ(i, 25);

    return 0;
}
