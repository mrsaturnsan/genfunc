#ifndef TEST_HPP
#define TEST_HPP

#include <exception>

template <typename T, typename U>
void EXPECT_EQ(const T& expression, const U& result)
{
    if (expression != result)
    {
        throw std::runtime_error{"EXPECT_EQ failed"};
    }
}

template <typename T, typename U>
void EXPECT_NEQ(const T& expression, const U& result)
{
    if (expression == result)
    {
        throw std::runtime_error{"EXPECT_NEQ failed"};
    }
}

#define EXPECT_NO_THROW(EXP)\
{\
    try\
    {\
        EXP\
    }\
    catch (...)\
    {\
        throw std::runtime_error{"EXPECT_NO_THROW failed"};\
    }\
}

#define EXPECT_THROW(EXP)\
{\
    bool caught = false;\
    try\
    {\
        EXP\
    }\
    catch (...)\
    {\
        caught = true;\
    }\
\
    if (!caught)\
    {\
        throw std::runtime_error{"EXPECT_THROW failed"};\
    }\
}


#endif
